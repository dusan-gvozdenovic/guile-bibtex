AM_SNARF_TEXINFOS = $(SOURCES:%.scm=$(top_builddir)/doc/%.texi)

AM_MAKEINFOHTMLFLAGS = \
    -c MENU_ENTRY_COLON="" \
    -c CONTENTS_OUTPUT_LOCATION='after_title' \
    -c AFTER_BODY_OPEN='<main class="content container">' \
    -c PRE_BODY_CLOSE='</main>' \
	--css-include="$(top_srcdir)/doc/css/main.css" \
	--css-include="$(top_srcdir)/doc/css/main-dark.css" \
	--css-include="$(top_srcdir)/doc/css/main-print.css" \
	--css-include="$(top_srcdir)/doc/css/code.css" \
	--css-include="$(top_srcdir)/doc/css/code-dark.css" \
	--css-include="$(top_srcdir)/doc/css/code-print.css"

info_TEXINFOS = doc/guile-bibtex.texi doc/fdl.texi

doc_guile_bibtex_TEXINFOS = $(AM_SNARF_TEXINFOS)

snarf_make_dirs:; mkdir -p $(AM_SNARF_DIRS)

# TODO: Finish this
make_css_dir:; mkdir -p $(top_builddir)/doc/css

$(AM_SNARF_TEXINFOS) : $(top_builddir)/doc/%.texi : $(top_srcdir)/%.scm | snarf_make_dirs
	guile-tools display-commentary $< | tail -n +2 > $@
	guile-tools doc-snarf -t $< >> $@
