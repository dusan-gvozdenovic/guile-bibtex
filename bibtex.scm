;; Copyright (C) 2021  Dušan Gvozdenović <dusan.gvozdenovic.99@gmail.com>
;;
;; This file is part of guile-bibtex
;;
;; guile-bibtex is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.
;;
;; guile-bibtex is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public License
;; along with guile-bibtex.  If not, see <http://www.gnu.org/licenses/>.

(define-module (bibtex)
  #:use-module (bibtex parser)
  #:use-module (bibtex semantics)
  #:use-module (bibtex database)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:export (bib-load))

(define (process-diagnostic diagnostic)
  (let ((source-range (bib-diagnostic-source-range diagnostic)))
    (format (current-error-port) "~a ~d:~d: ~a~%"
            (if (equal? (cadr diagnostic) 'error) "Error" "Warning")
            (bib-source-range-line (bib-diagnostic-source-range diagnostic))
            (bib-source-range-column (bib-diagnostic-source-range diagnostic))
            (bib-diagnostic-message diagnostic))))

; TODO: Handle bib-parse-exception
(define (bib-load port)
  (let* ((parse-info (bib-parse port))
         (tree (bib-parse-info-tree parse-info))
         (diagnostics (bib-check parse-info))
         (has-errors? (find (lambda (d) (equal? (cadr d) 'error)) diagnostics)))
    (for-each process-diagnostic diagnostics)
    (if has-errors? #f (bib-compile-database tree))))
