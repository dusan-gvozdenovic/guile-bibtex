;; Copyright (C) 2021  Dušan Gvozdenović <dusan.gvozdenovic.99@gmail.com>
;;
;; This file is part of guile-bibtex
;;
;; guile-bibtex is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.
;;
;; guile-bibtex is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public License
;; along with guile-bibtex.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; @node Semantic Checks
;; @section Semantic Checks
;;
;; This is a set of simple semantic checks performed by Guile BibTeX in order to
;; verify the correctness of parsed results and ensure that it adheres to the
;; suggested mode (TBD).
;;
;; @heading Procedures

;;; Code:

(define-module (bibtex semantics)
  #:use-module (bibtex parser)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:export (bib-diagnostic-level
            bib-diagnostic-message
            bib-diagnostic-source-range
            bib-check
            bib-check-duplicate-citekeys
            bib-check-duplicate-fields
            bib-check-entry
            bib-check-valid-entry
            bib-check-entry-required-fields
            bib-check-article
            bib-check-book))

; XXX: Problems with the following implementation are:
; 1. When fatal error is reached, we must stop all the checks and return the
;    current list of diagnostics together with the error.
; 2. When a field has an alias (e.g. author/editor in book entries) we should
;    not allow both to exist.
; 3. Try to break out of execution whenever a critical error is reported while
;    saving previous reports. Use call/cc or dynamic-wind
;
; Options: duplicate-citekeys
;          duplicate-fields
;          duplicate-aliased-fields
;          unrecognized-entry-type
;          required-fields
;
; A way to programmatically switch off these checks or add new ones should be
; made. This will allow us to have a tool and command line options later.
; (e.g. -Werror -Wduplicate-fields)
;
; Furthermore, checks should be organized into passes.

(define (make-bib-diagnostic level message position)
  `(bib-diagnostic ,level ,message ,position))

(define (bib-diagnostic-level diagnostic) (cadr diagnostic))

(define (bib-diagnostic-message diagnostic) (caddr diagnostic))

(define (bib-diagnostic-source-range diagnostic) (cadddr diagnostic))

; @b{TODO:} Document checks that bib-check performs under the hood
;; Performs simple semantic checks on the syntax tree @var{tree}.@*
(define (bib-check parse-info)
  (append (bib-check-duplicate-citekeys parse-info)
          (bib-check-duplicate-fields parse-info)))

;; Checks if any of the entries in the parse tree use the same citekey.
(define (bib-check-duplicate-citekeys parse-info)
  (let* ((table (make-hash-table))
         (check (lambda (entry)
                  (let ((citekey (bib-parse-tree-entry->citekey* entry)))
                          (if (hash-ref table citekey)
                            #t
                            (begin (hash-set! table citekey citekey) #f)))))
         (entries (bib-parse-tree->entries (bib-parse-info-tree parse-info)))
         (duplicate (find check entries)))
    (if duplicate
        (list (make-bib-diagnostic
          'error (format #f "Duplicate citekey ~s"
                         (bib-parse-tree-entry->citekey* duplicate))
          (hashq-ref (bib-parse-info-ranges parse-info)
                     ; TODO: Remove this cadr car cddddr non-sense!
                     (car (cddddr duplicate)))))
        '())))

;; Checks if any of the entries in the parse tree have a same field
;; specified twice.
(define (bib-check-duplicate-fields parse-info)
  (let* ((check (lambda (entry)
                  (bib-check-duplicate-fields-impl entry parse-info))))
    (apply append (map check (bib-parse-tree->entries
                               (bib-parse-info-tree parse-info))))))

(define (bib-check-duplicate-fields-impl entry-tree parse-info)
  (let* ((table (make-hash-table))
         (check-field
           (lambda (field)
             (if (hash-ref table (cadr field))
               #t
               (begin (hash-set! table (cadr field) field) #f))))
         (duplicate (find check-field (bib-parse-tree->fields entry-tree))))
    (if duplicate
        (list (make-bib-diagnostic
                'error
                (format #f "Duplicate field ~s" (cadadr duplicate))
                (hashq-ref (bib-parse-info-ranges parse-info)
                           (cadr (hash-ref table (cadr duplicate))))))
        '())))

(define (bib-check-entry-required-fields parse-info)
  (let* ((tree (bib-parse-info-tree parse-info))
         (entries (bib-parse-tree->entries tree)))
    (error "Not implemented yet!")))

; TODO: Define a style interface to specify and extend these things
; References
; https://mirror.marwan.ma/ctan/macros/latex/contrib/biblatex/doc/biblatex.pdf
; https://en.wikipedia.org/wiki/BibTeX#Entry_types
(define %default-entry-descriptions
  '((article (author title journal year volume)
             (number pages month doi note key))
    (book ((author editor) title publisher year)
          ((volume number) series address edition month note key url))
    (booklet (title)
             (author howpublished address month year note key))
    (inproceedings (author title booktitle year)
                   (editor (volume number) series pages address month
                           organization publisher note key))
    (manual (title)
            (author organization address edition month year note key))
    (mastersthesis (author title school year)
                   (type address month note key))
    (misc ()
          (author title howpublished month year note key))
    (phdthesis (author title school year)
               (type address month note key))
    (proceedings (title year)
                 (editor (volume number) series address month
                  organization publisher note key))
    (techreport (author title institution year)
                (type number address month note key))
    (unpublished (author title note) (month year key))

    ; From BibLaTeX
    (online ((author editor) title (year date) (doi eprint url))
            (subtitle titleaddon language version note organization month
             addendum pubstate eprintclass eprinttype urldate))))

;(define (bib-check-entry entry-tree) (bib-check-valid-entry entry-tree))

;;; Checks if an entry is properly formed.
;(define (bib-check-valid-entry entry-tree)
  ;(match entry-tree
         ;(('entry at type . rest)
          ;((or (assoc-ref %entry-checks (cadr type))
               ;(assoc-ref %entry-checks '*default*)) entry-tree))))

;; FIXME Rewrite this to work with parse tree
;(define (bib-check-entry-required-fields entry . fields)
  ;(define (check field)
    ;(if (list? field)
        ;(any (lambda (sub-field)
               ;(bib-check-entry-required-fields entry sub-field)) field)
      ;(bib-entry-field? entry field)))
  ;(every check fields)
  ;'())

;(define (bib-check-article entry)
  ;(bib-check-entry-required-fields entry
    ;'author 'title 'journal 'year 'volume))

;(define (bib-check-book entry)
  ;(bib-check-entry-required-fields entry
    ;'(author editor) 'title 'publisher 'year))
