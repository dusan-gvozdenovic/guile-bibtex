;;;; Copyright (C) 2021  Dušan Gvozdenović <dusan.gvozdenovic.99@gmail.com>
;;;;
;;;; This file is part of guile-bibtex
;;;;
;;;; guile-bibtex is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public License
;;;; as published by the Free Software Foundation, either version 3 of
;;;; the License, or (at your option) any later version.
;;;;
;;;; guile-bibtex is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU Lesser General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU Lesser General Public License
;;;; along with guile-bibtex.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;
;; @node Parser
;; @section Parser
;;
;; Guile BibTeX implements a simple recursive descent parser for a context-free
;; language similar to the one used in BibTeX @i{(.bib)} files. At the time of
;; writing an informal description of the format can be found
;; on @uref{http://www.bibtex.org/Format/}. There is also some information in
;; @uref{http://tug.ctan.org/info/bibtex/tamethebeast/ttb_en.pdf, Tame the BeaST}
;; which has yet to be explored.
;;
;; @subsection Grammar
;;
;;@verbatim
;;START       -> ENTRY | START
;;ENTRY       -> '@' IDENTIFIER '{' IDENTIFIER ',' KEYS '}'
;;FIELDS      -> FIELD | FIELD ',' FIELDS
;;FIELD       -> IDENTIFIER = VALUE
;;VALUE       -> NUMBER | '"' STRING '"' | '{' STRING '}'
;;IDENTIFIER  -> ([:alnum:]|[+-_<>])+
;;NUMBER      -> [:digit:]+
;;STRING      -> [^"]+
;;@end verbatim
;;
;; Whitespaces and comments in the form of @code{%[^\n]*$} can appear between
;; any of the lexemes and are not considered to be a part of the language.
;;
;; @heading Procedures
;;

;;; Code:

(define-module (bibtex parser)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (rnrs io ports)
  #:use-module (srfi srfi-9)
  #:export (bib-parser
            bib-parser?
            bib-parser-port
            bib-parser-ranges
            bib-parser-trivia
            bib-parser-current-trivia
            bib-parser-set-current-trivia!
            bib-parse-info
            bib-parse-info?
            bib-parse-info-tree
            bib-parse-info-ranges
            bib-parse-info-trivia
            make-bib-parser
            bib-source-range
            bib-source-range?
            bib-source-range-line
            bib-source-range-column
            bib-source-range-position
            bib-source-range-length
            bib-parse-catch
            bib-parse
            bib-parse-entry
            bib-parse-fields
            bib-parse-field
            bib-parse-identifier
            bib-parse-number
            bib-parse-string
            bib-parse-tree->fields
            bib-parse-tree->entries
            bib-parse-tree-entry->citekey
            bib-parse-tree-entry->citekey*))

; ----------------------------- Parser structure ----------------------------- ;

; TODO: Document these types

(define-record-type bib-parser
  (make-bib-parser-internal port ranges trivia current-trivia)
  bib-parser?
  (port bib-parser-port)
  (ranges bib-parser-ranges)
  (trivia bib-parser-trivia)
  (current-trivia bib-parser-current-trivia bib-parser-set-current-trivia!))

(define (make-bib-parser port)
  (make-bib-parser-internal port (make-hash-table) (make-hash-table) ""))

(define-record-type bib-source-range
  (make-bib-source-range line column position len)
  bib-source-range?
  (line bib-source-range-line)
  (column bib-source-range-column)
  (position bib-source-range-position)
  (len bib-source-range-length))

; TODO: Consider refactoring bib-parser to directly use bib-parse-info
(define-record-type bib-parse-info
  (make-bib-parse-info tree ranges trivia)
  bib-parse-info?
  (tree bib-parse-info-tree)
  (ranges bib-parse-info-ranges)
  (trivia bib-parse-info-trivia))

; ------------------------------ Error handling ------------------------------ ;

(define (bib-parse-throw message parser)
  (let ((port (bib-parser-port parser)))
    (throw 'bib-parse-exception
           (format #f "~d:~d: ~a" (port-line port)
                                  (port-column port) message))))

;; This is a syntax rule which takes a @var{thunk} and an @var{error-handler}.
;; It executes @var{thunk} and breaks into @var{error-handler} only if
;; @code{'bib-parse-exception} is thrown. @*
;; @b{Note:} Because of Guile's transition to new exception handling mechanism
;; this the recommended way of catching parsing errors to maintain the
;; compatibility between versions 2.0 and 3.0.
;; @example
;; (bib-parse-catch (lambda () (bib-parse port))
;;                  (lambda (message) (format (current-error-port)
;;                                            "Syntax error: ~a\n" message)))
;; @end example
(define-syntax bib-parse-catch
  (syntax-rules ()
    ((_ thunk error-handler)
      (catch 'bib-parse-exception thunk
             (lambda (key message) (error-handler message))))))

; ---------------------------- Reading utilities ----------------------------- ;

(define (bib-read-while parser pred)
  (let* ((port (bib-parser-port parser))
         (stop (lambda (c) (or (eof-object? (peek-char port))
                              (not (pred (peek-char port))))))
         (gen  (lambda (c) (read-char port))))
    (string-unfold stop gen string-append "")))

; TODO: Add %-comments and update docs
(define (bib-skip-non-grammar parser)
  (bib-parser-set-current-trivia! parser
    (string-append (bib-parser-current-trivia parser)
                   (bib-read-while parser char-whitespace?))))

(define (bib-eoi? parser)
  (bib-skip-non-grammar parser)
  (eof-object? (peek-char (bib-parser-port parser))))

(define (bib-peek-next parser)
  (bib-skip-non-grammar parser)
  (let* ((c (peek-char (bib-parser-port parser))))
    (when (eof-object? c)
      (bib-parse-throw "Unexpected end of input." parser))
    c))

(define (bib-expect-pred predicate message parser)
  (unless (predicate (bib-peek-next parser)) (bib-parse-throw message parser)))

(define (bib-expect-char char parser)
  (bib-expect-pred
    (lambda (c) (char=? char c))
    (format #f "Expected a '~c' character!" char) parser))

; --------------------------- Parser implementation -------------------------- ;

;; Returns a @code{(parse-info parse-tree source-ranges trivia)} s-expr where:@*
;; 1. @var{parse-tree} --- represents valid BibTeX expressions that were read
;;    from the @var{port}.@*
;; 2. @var{source-ranges} --- is a hash table which maps tokens from the parse
;;    tree to their positions in the source code.@*
;; 3. @var{trivia} -- is a hash table which maps a token from the parse tree
;;    to a non-grammar string (whitespaces and comments) which was read before
;;    it.@*
;; @b{Remark:} When working with @var{trivia} and @var{source-ranges} be careful
;; to use the exact tokens from the tree as they are hashed by their memory
;; location.@*
;; Throws @code{'bib-parse-exception} together with the error message
;; on failure.@*
(define (bib-parse port)
  (let* ((parser (make-bib-parser port))
         (result (bib-parse-start parser))
         (ranges (bib-parser-ranges parser))
         (trivia (bib-parser-trivia parser)))
    (make-bib-parse-info result ranges trivia)))

(define (bib-parse-start parser)
  (cond
    ((bib-eoi? parser) '())
    ((char=? (bib-peek-next parser) #\@)
     (cons (bib-parse-entry parser) (bib-parse-start parser)))
    (else (bib-parse-throw "Expected a '@' character!" parser))))

(define (bib-parse-entry parser)
  `(entry ,(bib-parse-special parser #\@)
          ,(bib-parse-identifier parser)     ;; Entry type
          ,(bib-parse-special parser #\{)
          ,(bib-parse-identifier parser)     ;; Citekey
          ,(bib-parse-special parser #\,)
          ,(bib-parse-fields parser)         ;; Fields
          ,(bib-parse-special parser #\})))

(define (bib-parse-fields parser)
  (let* ((field (bib-parse-field parser))
         (c (bib-peek-next parser)))
    (cond
      ((char=? c #\}) (cons field '()))
      ((char=? c #\,) (cons field (cons (bib-parse-special parser #\,)
                                        (bib-parse-fields parser))))
      (else (bib-parse-throw (format #f "Invalid character '~c'" c) parser)))))

(define (bib-parse-field parser)
  (let* ((identifier (bib-parse-identifier parser))
         (assignment (bib-parse-special parser #\=))
         (value (bib-parse-value parser)))
      `(field ,identifier ,assignment ,value)))

(define (bib-parse-value parser)
  (let* ((c (bib-peek-next parser)))
    (cond ((char-numeric? c) (bib-parse-number parser))
          ((or (char=? c #\") (char=? c #\{)) (bib-parse-string parser))
          (else (bib-parse-throw (format #f "Expected a number or a string.")
                                 parser)))))

; TODO: Make range format nicer
; TODO: Add docs
(define-syntax define-token
  (syntax-rules ()
    ((define-token (name parser params ...) body ...)
     (define (name parser params ...)
       (bib-skip-non-grammar parser)
       (let* ((name (lambda (parser params ...) (begin body ...)))
              (_port_ (bib-parser-port parser))
              (_start_line_ (port-line _port_))
              (_start_column_ (port-column _port_))
              (_start_position_ (port-position _port_))
              (_value_ (name parser params ...))
              (_end_position_ (port-position _port_))
              (_curr_trivia_ (bib-parser-current-trivia parser)))
         ;; Insert ranges
         (hashq-set! (bib-parser-ranges parser) _value_
                     (make-bib-source-range
                       _start_line_ _start_column_ _start_position_
                       (- _end_position_ 1 _start_position_)))
         ;; Set and clear trivia
         (hashq-set! (bib-parser-trivia parser) _value_ _curr_trivia_)
         (bib-parser-set-current-trivia! parser "")
         _value_)))))

(define-token (bib-parse-special parser char)
  (bib-expect-char char parser)
  `(special ,(get-char (bib-parser-port parser))))

; TODO: Investigate the exact class used in other BibTeX implementations
(define (char-identifier? c)
  (or (char-alphabetic? c) (char-numeric? c)
      (equal? c #\+) (equal? c #\-) (equal? c #\_) (equal? c #\<) (equal? c #\>)
  ))

(define-token (bib-parse-identifier parser)
  (bib-expect-pred char-identifier? "Expected an identifier!" parser)
  `(identifier ,(bib-read-while parser char-identifier?)))

(define-token (bib-parse-number parser)
  (bib-expect-pred char-numeric? "Expected a number!" parser)
  `(number ,(string->number (bib-read-while parser char-numeric?))))

; TODO: Extend this to support more complex strings
(define-token (bib-parse-string parser)
  (let* ((open (bib-peek-next parser))
         (close (if (char=? open #\{) #\} #\")))
    (bib-parse-special parser open)
    (let ((value (bib-read-while parser (lambda (c) (not (char=? close c))))))
      (bib-parse-special parser close)
      `(string ,value))))

; --------------------------- Parse tree utilities --------------------------- ;

;; Filters all entries from the parse tree starting from entry node
(define bib-parse-tree->fields
  (match-lambda (('entry at type open citekey comma fields close)
    (filter (match-lambda
           (('field identifier equal value) #t)
           (_ #f)) fields))))

;; Filters all entries from the parse tree
(define (bib-parse-tree->entries parse-tree)
  (filter (match-lambda
            (('entry at type open citekey comma fields close) #t)
            (_ #f)) parse-tree))

(define bib-parse-tree-entry->citekey
  (match-lambda ((entry at type open citekey comma fields close) citekey)))

(define bib-parse-tree-entry->citekey*
  (match-lambda ((entry at type open citekey comma fields close)
                 (cadr citekey))))
