;; Copyright (C) 2021  Dušan Gvozdenović <dusan.gvozdenovic.99@gmail.com>
;;
;; This file is part of guile-bibtex
;;
;; guile-bibtex is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.
;;
;; guile-bibtex is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public License
;; along with guile-bibtex.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; @node Entries and Databases
;; @section Entries and Databases
;;
;; @cindex Entry
;; BibTeX entries are represented with a
;; @url{https://srfi.schemers.org/srfi-9/srfi-9.html, SRFI-9} record type
;; consisting of:
;; @enumerate
;; @item @code{type} --- a symbol representing the type of the BibTeX entry
;; @item @code{citekey} --- a unique string by which the entry is identified
;; @item @code{fields} --- an alist of keys and values
;; @end enumerate

;;@example
;;(define-record-type bib-entry
;;  (make-bib-entry type citekey fields)
;;  bib-entry?
;;  (type bib-entry-type)
;;  (citekey bib-entry-citekey)
;;  (fields bib-entry-fields))
;;@end example

;; @heading Databases
;; @cindex Database
;; TODO: Document later

;; @heading Procedures

;;; Code:

(define-module (bibtex database)
  #:use-module (bibtex parser)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 match)
  #:export (bib-entry
            make-bib-entry
            bib-entry?
            bib-entry-type
            bib-entry-citekey
            bib-entry-fields
            make-bib-entry-field
            bib-entry-field-name-equal?
            bib-entry-field?
            bib-entry-field
            bib-compile-database
            bib-database-lookup))

; ----------------------------------- Entry ---------------------------------- ;

(define-record-type bib-entry
  (make-bib-entry type citekey fields)
  bib-entry?
  (type bib-entry-type)
  (citekey bib-entry-citekey)
  (fields bib-entry-fields))

(define (make-bib-entry-field key value) `(,key . ,value))

;; Compares field names @var{f1} and @var{f2} for equality, ignoring the case.
(define (bib-entry-field-name-equal? f1 f2)
  (string-ci=? (symbol->string f1) (symbol->string f2)))

;; Returns @code{#t} if @var{entry} has a field named @var{name}. Otherwise
;; returns @code{#f}.
(define (bib-entry-field? entry name)
  (not (not (assoc name (bib-entry-fields entry) bib-entry-field-name-equal?))))

;; Searches for the @var{name} in @var{entry} fields and returns its associated
;; value.
;; Returns @code{#f} if a field with the given @var{name} does not exist.
(define (bib-entry-field entry name)
  "Searches for the NAME in ENTRY fields and returns its associated value.
   Return #f if a field with the given NAME does not exist."
  ; TODO: Properly convert symbol to string and pass that
  (unless (or (symbol? name) (string? name))
    (error "Expected a symbol or a string in place of 'name"))
  (cdr (assoc name (bib-entry-fields entry) bib-entry-field-name-equal?)))

; ---------------------------- Parse tree utilities -------------------------- ;

; TODO: Try to reuse fields instead of double matching.
(define (entry-from-parse-tree parse-tree)
  "Converts PARSE-TREE entry to a bib-entry record."
  (match parse-tree
         (('entry at type open citekey comma fields close)
          (make-bib-entry (cadr type)
                          (cadr citekey)
                          (entry-fields-from-parse-tree parse-tree)))))

(define (entry-fields-from-parse-tree parse-tree)
  "Extracts all field s-expressions from PARSE-TREE entry and converts them to
   an alist"
  (map (lambda (f) `(,(cadr (cadr f)) . ,(cadr (cadddr f))))
       (bib-parse-tree->fields parse-tree)))

; ---------------------------------- Database -------------------------------- ;

(define-record-type bib-database
  (make-bib-database entries)
  bib-database?
  (entries bib-database-entries))

;; Returns a queryable database compiled from @var{parse-tree}.
(define (bib-compile-database parse-tree)
  "Returns a queryable database compiled from TREE."
  (let ((table (make-hash-table))
        (entries (map entry-from-parse-tree
                      (bib-parse-tree->entries parse-tree))))
    (for-each (lambda (e) (hash-set! table (bib-entry-citekey e) e)) entries)
  (make-bib-database table)))

;; Looks up @var{citekey} (symbol or a string) in @var{db} and returns the
;; associated entry.
;; Returns @code{#f} if no key is found.
(define (bib-database-lookup db citekey)
  (unless (or (symbol? citekey) (string? citekey))
    (error "Expected a symbol or a string in place of 'citekey'"))
  (hash-ref (bib-database-entries db)
            (if (symbol? citekey) (symbol->string citekey) citekey)))
