# Guile BibTeX

Library for working with BibTeX databases

## Building and installation

This library depends only on Autotools and Guile (>= 2.0).

```bash
./bootstrap
mkdir build && cd build
../configure
make
make install
```

## Usage and documentation

Latest documentation can be found at <https://dusan-gvozdenovic.gitlab.io/guile-bibtex>.

## Tests

Use `make check` to run the tests.

## License

LGPLv3 or later. See COPYING.LESSER and COPYING
