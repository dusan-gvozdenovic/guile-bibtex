;; Copyright (C) 2021  Dušan Gvozdenović <dusan.gvozdenovic.99@gmail.com>
;;
;; This file is part of guile-bibtex
;;
;; guile-bibtex is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.
;;
;; guile-bibtex is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public License
;; along with guile-bibtex.  If not, see <http://www.gnu.org/licenses/>.

(define-module (bibtex database-test)
   #:use-module (ice-9 textual-ports)
   #:use-module (srfi srfi-1)
   #:use-module (srfi srfi-64)
   #:use-module (bibtex database)
   #:use-module (bibtex parser))

(test-begin "database")

; TODO: Finish this
(test-assert "compile database" #t)

(test-end)
