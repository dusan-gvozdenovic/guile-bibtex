;; Copyright (C) 2021  Dušan Gvozdenović <dusan.gvozdenovic.99@gmail.com>
;;
;; This file is part of guile-bibtex
;;
;; guile-bibtex is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.
;;
;; guile-bibtex is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public License
;; along with guile-bibtex.  If not, see <http://www.gnu.org/licenses/>.

(define-module (bibtex parser-test)
   #:use-module (ice-9 textual-ports)
   #:use-module (srfi srfi-1)
   #:use-module (srfi srfi-64)
   #:use-module (bibtex parser))

(define-syntax test-grammar-rule
  (syntax-rules ()
    ((test-grammar-rule name rule input-string expect)
     (test-equal name expect
       (call-with-input-string input-string
         (lambda (port) (rule (make-bib-parser port))))))))

; TODO: simplify this
(define-syntax test-grammar-fail
  (syntax-rules ()
    ((test-grammar-rule-fail name rule input-string expect ...)
     (let* ((inner (lambda (port)
      (let ((parser (make-bib-parser port)))
        (bib-parse-catch
          (lambda () (rule parser) #f)
          (lambda (message)
            (fold (lambda (curr acc) (and curr acc)) #t
                  (map (lambda (condition) (condition parser)) `(,expect ...)))))))))
       (test-assert name (call-with-input-string input-string inner))))))

(test-begin "parser")

(test-grammar-rule "parse alphanumeric identifier"
  bib-parse-identifier "  11aas4dbdf5g  " '(identifier "11aas4dbdf5g"))

(test-grammar-fail "invalid alphanumeric identifier"
  bib-parse-identifier "   #%f1f23%#$# "
  (lambda (parser) (equal? (peek-char (bib-parser-port parser)) #\#)))

(test-grammar-rule "parse number"
  bib-parse-number "   349750   " '(number 349750))

(test-grammar-fail "parse invalid number"
  bib-parse-number "   asdf "
  (lambda (parser) (equal? (peek-char (bib-parser-port parser)) #\a)))

(test-grammar-rule "parse empty string"
  bib-parse-string "{}" '(string ""))

(test-grammar-rule "parse simple string"
  bib-parse-string "\"   @493 this is a test!@  \""
  '(string "   @493 this is a test!@  "))

(test-grammar-rule "parse field"
  bib-parse-field "a = 5"
  '(field (identifier "a") (special #\=) (number 5)))

(test-grammar-rule "parse simple entry"
  bib-parse-entry
  "@book{SWH88,\
       title     = {A Brief History of Time: From the Big Bang to Black Holes},\
       author    = {Hawking, Stephen},\
       year      = 1988,\
       publisher = {Bantam},\
       address   = {London}\
   }"
  '(entry
    (special #\@) (identifier "book") (special #\{) (identifier "SWH88")
    (special #\,)
    ((field (identifier "title") (special #\=)
            (string "A Brief History of Time: From the Big Bang to Black Holes"))
     (special #\,)
     (field (identifier "author") (special #\=)
            (string "Hawking, Stephen"))
     (special #\,)
     (field (identifier "year") (special #\=)
            (number 1988))
     (special #\,)
     (field (identifier "publisher") (special #\=)
            (string "Bantam"))
     (special #\,)
     (field (identifier "address") (special #\=)
            (string "London")))
    (special #\})))

(test-end)
