;; Copyright (C) 2021  Dušan Gvozdenović <dusan.gvozdenovic.99@gmail.com>
;;
;; This file is part of guile-bibtex
;;
;; guile-bibtex is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.
;;
;; guile-bibtex is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public License
;; along with guile-bibtex.  If not, see <http://www.gnu.org/licenses/>.

(define-module (bibtex semantics-test)
   #:use-module (ice-9 textual-ports)
   #:use-module (srfi srfi-1)
   #:use-module (srfi srfi-64)
   #:use-module (bibtex parser)
   #:use-module (bibtex semantics))

(define (execute-check check source)
  (check (call-with-input-string source bib-parse)))

(test-begin "semantics")

; TODO: Test if reported source location is correct
(test-assert "duplicate citekeys"
  (let ((diagnostics (execute-check bib-check-duplicate-citekeys
                        "@misc{citekey, a = 5 } @book{citekey, b = 6}")))
    (and (equal? (length diagnostics) 1)
         (equal? (bib-diagnostic-level (car diagnostics)) 'error)
         (equal? (bib-diagnostic-message (car diagnostics))
                 "Duplicate citekey \"citekey\""))))

(test-equal "no duplicate citekeys" '()
  (execute-check bib-check-duplicate-citekeys
  "@misc{citekey, a = 5} @book{citekey2, b = 6}"))

; TODO: Test if reported source location is correct
(test-assert "duplicate fields"
  (let ((diagnostics (execute-check bib-check-duplicate-fields
                       "@misc{citekey, a = 5, a = 6}")))
    (and (equal? (length diagnostics) 1)
         (equal? (bib-diagnostic-level (car diagnostics)) 'error)
         (equal? (bib-diagnostic-message (car diagnostics))
                 "Duplicate field \"a\""))))

(test-equal "no duplicate fields" '()
  (execute-check bib-check-duplicate-fields
  "@misc{citekey, a = 5, b = 6} @book{citekey2, b = 6}"))

(test-end)
